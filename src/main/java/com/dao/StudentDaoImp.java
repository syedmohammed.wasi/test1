package com.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class StudentDaoImp implements StudentDao {
	private Connection connection;

	public StudentDaoImp() {
		connection = ConnectionFactory.getConnection();
	}
	public List<Student> getAllStudent() {
		List<Student> students=new ArrayList<Student>();
		Student student;
		
		try {
			PreparedStatement pstmt = connection.prepareStatement("select * from student ");
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				student = new Student(rs.getInt("id"), 
						rs.getString("name"), 
						rs.getInt("marks") 
						);
				
				students.add(student);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return students;
		
	}

	
{

}

	
}
